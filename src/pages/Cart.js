import { useContext } from "react"
import CartProduct from "../components/CartProduct"
import UserContext from "../context/UserContext"

const Cart = () => {

    const { cart, totalPrice, setCart, setTotalPrice, setCartTotal } = useContext(UserContext)
    const clearCart = () => {
        setCart([])
        setTotalPrice(0)
        setCartTotal(0)
    }
    
  return (
    <div style={{ width: "100%" }}>
        <div className="dashboard-content-container">
            <div className="dashboard-top-section">
                <h1>Total Price: ¥{totalPrice}</h1>
                <div className="dashboard-top-section-card">
                    <div className="relative h-full w-full">
                        <div className="mt-8 mb-20 flex flex-wrap fluid-buttons justify-between">
                            <div className="dashboard-button-link">
                                <button onClick={clearCart} className="dashboard-button">Checkout</button>
                            </div>
                            <div className="dashboard-button-link">
                                <button onClick={clearCart} className="dashboard-button">Clear Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="dashboard-products-list">
                {cart.map(item => {
                    return (
                        <CartProduct product={item} />
                    )
                })}
            </div>
        </div>
    </div>
  )
}
export default Cart