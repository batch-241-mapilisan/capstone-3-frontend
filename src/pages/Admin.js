import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import DashboardProduct from "../components/DashboardProduct"

const Admin = () => {

    const [createProductForm, setCreateProductForm] = useState(false)
    const [allProducts, setAllProducts] = useState(false)
    const [productId, setProductId] = useState(0)
    const [productName, setProductName] = useState("")
    const [series, setSeries] = useState("")
    const [manufacturer, setManufacturer] = useState("")
    const [category, setCategory] = useState("")
    const [price, setPrice] = useState(0)
    const [releaseDate, setReleaseDate] = useState("")
    const [specifications, setSpecifications] = useState("")
    const [isError, setIsError] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')
    const [products, setProducts] = useState([])
    
    const productsList = products.map(product => {
        return (
            <DashboardProduct key={product._id} product={product} />
        )
    })

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products`)
        .then(res => res.json())
        .then(data => {

            setProducts(data.sort((a, b) => {
                return b.productId - a.productId
            }))
        })
    }, [])

    const showCreateProduct = () => {
        setAllProducts(false)
        setCreateProductForm(true)
    }

    const showAllProducts = () => {
        setCreateProductForm(false)
        setAllProducts(true)
    }

    const createProduct = (e) => {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId,
                productName,
                series,
                manufacturer,
                category,
                price,
                releaseDate,
                specifications
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if (data.status !== "success") {
                setIsError(true)
                setErrorMessage(data.message)
            } else {
                setIsError(false)
                setProductId(0)
                setProductName("")
                setSeries("")
                setManufacturer("")
                setCategory("")
                setPrice(0)
                setReleaseDate("")
                setSpecifications("")
                setCreateProductForm(false)
            }
        })
    }

  return (
    <div className="dashboard-content-container">

        <div className="dashboard-top-section">
            <h1>Admin Dashboard</h1>
            <div className="dashboard-top-section-card">
                <div className="relative h-full w-full">
                    <div className="mt-8 mb-20 flex flex-wrap fluid-buttons justify-between">
                        <Link to="#" onClick={showCreateProduct} className="dashboard-button-link">
                            <button className="dashboard-button">Create Product</button>
                        </Link>
                        <Link to="#" onClick={showAllProducts} className="dashboard-button-link">
                            <button className="dashboard-button">Retrieve All Products</button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>

        {
            createProductForm
            &&
            <div className="px-1 py-6 relative flex flex-col items-center justify-between overflow-x-hidden overflow-y-hidden">
            <div className="pt-10 w-11/12 max-w-md">
                <form className="px-5 py-8 rounded-lg form-bg border border-solid border-gray-800">
                    <div className="mb-5">
                        <div className="relative">
                            <input
                                type="number"
                                value={productId}
                                onChange={e => setProductId(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Product ID</label>
                        </div>
                    </div>

                    <div className="mb-5">
                        <div className="relative">
                            <input 
                                type="text"
                                value={productName}
                                onChange={e => setProductName(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Product Name</label>
                        </div>
                    </div>

                    <div className="mb-5">
                        <div className="relative">
                            <input 
                                type="text"
                                value={series}
                                onChange={e => setSeries(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Series</label>
                        </div>
                    </div>

                    <div className="mb-5">
                        <div className="relative">
                            <input 
                                type="text"
                                value={manufacturer}
                                onChange={e => setManufacturer(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Manufacturer</label>
                        </div>
                    </div>

                    <div className="mb-5">
                        <div className="relative">
                            <input 
                                type="text"
                                value={category}
                                onChange={e => setCategory(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Category</label>
                        </div>
                    </div>

                    <div className="mb-5">
                        <div className="relative">
                            <input 
                                type="number"
                                value={price}
                                onChange={e => setPrice(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Price</label>
                        </div>
                    </div>

                    <div className="mb-5">
                        <div className="relative">
                            <input 
                                type="text"
                                value={releaseDate}
                                onChange={e => setReleaseDate(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Release Date</label>
                        </div>
                    </div>

                    <div className="mb-16">
                        <div className="relative">
                            <input 
                                type="text"
                                value={specifications}
                                onChange={e => setSpecifications(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Specifications</label>
                        </div>
                    </div>

                    {
                        isError
                        &&
                        <div className="my-2 text-center text-red-600 text-sm font-normal leading-5">
                            {errorMessage}
                        </div>
                    }

                    <button
                        onClick={e => createProduct(e)} 
                        className="button-gradient text-sm h-10 leading-5 px-4 w-full border border-transparent items-center rounded-lg text-white inline-flex font-medium justify-center relative text-center"
                    >
                        Create Product
                    </button>
                </form>
            </div>
            </div>
        }

        {
            products && allProducts
            &&
            <div className="dashboard-products-list">
                {productsList}
            </div>
        }
    </div>
  )
}
export default Admin