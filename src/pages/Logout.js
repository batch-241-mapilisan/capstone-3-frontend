import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../context/UserContext";

const Logout = () => {

    const { unsetUser, setUser } = useContext(UserContext)

    unsetUser()

    useEffect(() => {
        setUser({
            userId: null,
            username: null,
            isAdmin: null
        })
    })

  return (
    <Navigate to='/' />
  )
}
export default Logout