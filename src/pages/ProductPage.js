import { useContext, useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import ProductsHeader from "../components/ProductsHeader"
import UserContext from "../context/UserContext"

const ProductPage = () => {

    const { id } = useParams()
    const { user, setCart, setCartTotal, cart, setTotalPrice } = useContext(UserContext)
    console.log(user)
    const [product, setProduct] = useState({})
    const [dataLoaded, setDataLoaded] = useState(false)

    const addToCart = (id) => {
        setCart([...cart, {id, name: product.productName, price: product.price, quantity: 1}])
        setCartTotal(prev => prev + 1)
        setTotalPrice(prev => prev + product.price)
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
        .then(res => res.json())
        .then(data => {
            setProduct(data)
            setDataLoaded(true)
        })
    }, [id])

    let addToCartButton
    if (user.userId == null || user.isAdmin) {
        addToCartButton = (
            <button disabled className="nendoroid-productPage-buy-button">
                Add to Cart
            </button>
        )
    } else {
        addToCartButton = (
            <button onClick={() => addToCart(id)} className="nendoroid-productPage-buy-button">
                Add to Cart
            </button>
        )
    }

  return (
    (dataLoaded) 
    ?
    <div className="product-main-layout-container">
        <ProductsHeader />
        <div className="nendoroid-content-wrapper">
            <div>
                <div className="nendoroid-details-container">

                    <div className="nendoroid-details-avatar">

                        <div className="nendoroid-avatar-header">
        
                            <div className="nendoroid-avatar-name">
                                <div className="nendoroid-avatar-productId">
                                    <div className="nendoroid-productId">
                                        <span>#{product.productId}</span>
                                    </div>
                                </div>

                                <div>
                                    <div className="nendoroid-name">
                                        <h1>{product.productName}</h1>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div className="nendoroid-avatar-figure">
                            <div className="nendoroid-avatar-figure-container">
                                <img className="nendoroid-avatar-figure-image" src={ require(`../assets/images/${product.productId}.png`) } alt={product.productName} />
                            </div>
                        </div>
                    </div>

                    <div className="nendoroid-avatar-details-section">
                        <div className="w-full">
                            <div className="nendoroid-avatar-details-section-price-info">
                                <h4 className="nendoroid-avatar-details-section-price-h4">¥{product.price}</h4>
                                {addToCartButton}
                            </div>
                        </div>

                        <div className="mt-10">
                            <div className="nendoroid-productPage-about">
                                <div className="flex flex-col items-start justify-start">
                                    <div className="nendoroid-productPage-about-productField">
                                        Product ID
                                    </div>
                                    <div>Nendoroid {product.productId}</div>
                                </div>
                                <div className="flex flex-col items-start justify-start mt-6">
                                    <div className="nendoroid-productPage-about-productField">
                                        Product Name
                                    </div>
                                    <div>{product.productName}</div>
                                </div>
                                <div className="flex flex-col items-start justify-start mt-6">
                                    <div className="nendoroid-productPage-about-productField">
                                        Series
                                    </div>
                                    <div>{product.series}</div>
                                </div>
                                <div className="flex flex-col items-start justify-start mt-6">
                                    <div className="nendoroid-productPage-about-productField">
                                        Manufacturer
                                    </div>
                                    <div>{product.manufacturer}</div>
                                </div>
                                <div className="flex flex-col items-start justify-start mt-6">
                                    <div className="nendoroid-productPage-about-productField">
                                        Category
                                    </div>
                                    <div>{product.category}</div>
                                </div>
                                <div className="flex flex-col items-start justify-start mt-6">
                                    <div className="nendoroid-productPage-about-productField">
                                        Release Date
                                    </div>
                                    <div>{product.releaseDate}</div>
                                </div>
                                <div className="flex flex-col items-start justify-start mt-6">
                                    <div className="nendoroid-productPage-about-productField">
                                        Specifications
                                    </div>
                                    <div>{product.specifications}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    :
    <div></div>
  )
}
export default ProductPage