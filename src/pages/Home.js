import { useContext } from "react"
import { Link } from "react-router-dom"
import UserContext from "../context/UserContext"


const Home = () => {

  const { user } = useContext(UserContext)

  return (
    <main className="home-main">
        <div className="home-container-content">
            
            <div className="home-hero-banner-container">
              <div className="home-hero-banner-header">
                <div className="home-hero-banner-header-info-section">
                  <h4>Welcome to Nendoro</h4>
                  {
                    user.userId !== null
                    ?
                    <Link to="/logout"><button>Log out</button></Link>
                    :
                    <Link to="/login"><button>Log in</button></Link>
                  }
                </div>
              </div>
            </div>
            
            <div style={{marginBottom: "20px"}} className="home-hero-picture">
              <img className="hero-picture" src={require('../assets/images/hero-picture.webp')} alt="hero pic" />
              <Link to="/products"><button>View All Products</button></Link>
            </div>

            <div className="home-hero-picture">
              <img className="hero-picture" src={require('../assets/images/view-products.webp')} alt="hero pic" />
              <Link to="/products"><button>Browse All Products</button></Link>
            </div>

        </div>
    </main>
  )
}
export default Home