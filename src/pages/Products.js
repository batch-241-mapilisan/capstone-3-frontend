import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import ProductCard from "../components/ProductCard"
import ProductsHeader from "../components/ProductsHeader"

const Products = () => {

    const [products, setProducts] = useState([])
    
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/active`)
        .then(res => res.json())
        .then(data => {

            setProducts(data.sort((a, b) => {
                return b.productId - a.productId
            }))
        })
    }, [])

    const productsList = products.map(product => {
        return (
            <Link key={product._id} to={`/products/${product._id}`}><ProductCard key={product._id} product={product} /></Link>
        )
    })

  return (
    <div className="products-content-container">

        <ProductsHeader />

        <div className="products-content">
            <div className="products-content-listing-wrapper">
                <div className="products-content-listing">
                    <header className="products-listing-header">
                        <h4 className="products-listing-h4">
                            {products.length} Nendoroids
                        </h4>
                    </header>

                    <div className="products-listing-container">
                        <div className="products-grid">
                            {products && productsList}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
  )
}
export default Products