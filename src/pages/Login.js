import { useContext, useState } from "react"
import UserContext from "../context/UserContext"
import jwtDecode from 'jwt-decode'
import { Link, Navigate } from "react-router-dom"

const Login = () => {

    const { user, setUser } = useContext(UserContext)

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [isError, setIsError] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')

    const authenticate = (e) => {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/auth`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data.status !== "success") {
                setIsError(true)
                setErrorMessage(data.message)
            } else {
                localStorage.setItem("token", data.accessToken)
                const decoded = jwtDecode(data.accessToken)
                const { userId, username, isAdmin } = decoded.UserInfo
                setUser({
                    userId,
                    username,
                    isAdmin
                })
            }
        })
    }

  return (
    (user.userId !== null) ? <Navigate to='/' />
    :
    <>

    <div className="px-1 py-6 relative flex flex-col items-center justify-between overflow-x-hidden overflow-y-hidden">
        <div className="pt-10 w-11/12 max-w-md">
            <form className="px-5 py-8 rounded-lg form-bg border border-solid border-gray-800">
                <div className="mb-5">
                    <div className="relative">
                        <input
                            value={username}
                            onChange={e => setUsername(e.target.value)} 
                            required 
                            className="input-focus" 
                        />
                        <label className="input-label">Username</label>
                    </div>
                </div>

                <div className="mb-16">
                    <div className="relative">
                        <input 
                            type="password"
                            value={password}
                            onChange={e => setPassword(e.target.value)} 
                            required 
                            className="input-focus" 
                        />
                        <label className="input-label">Password</label>
                    </div>
                </div>

                {
                    isError
                    &&
                    <div className="my-2 text-center text-red-600 text-sm font-normal leading-5">
                        {errorMessage}
                    </div>
                }

                <button
                    onClick={e => authenticate(e)} 
                    className="button-gradient text-sm h-10 leading-5 px-4 w-full border border-transparent items-center rounded-lg text-white inline-flex font-medium justify-center relative text-center"
                >
                    Sign in
                </button>
            </form>
        </div>
    </div>
    
    <div className="register-text">Don't have an account yet?<Link className="register-link" to="/register">Register here</Link></div>
    </>
  )
}
export default Login