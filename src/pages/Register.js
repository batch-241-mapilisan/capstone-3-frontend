import { useContext, useEffect, useState } from "react"
import { Navigate, useNavigate } from "react-router-dom"
import UserContext from "../context/UserContext"

const Register = () => {

    const { user } = useContext(UserContext)

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [repeatPassword, setRepeatPassword] = useState('')
    const [isEnabled, setIsEnabled] = useState(false)
    const [isError, setIsError] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')

    const navigate = useNavigate()

    const register = (e) => {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username,
                password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if (data.status !== "success") {
                setIsError(true)
                setErrorMessage(data.message)
            } else {
                setIsError(false)
                navigate("/login")
            }
        })
    }

    useEffect(() => {
        if (password === repeatPassword && password !== '' && username !== '') {
            setIsEnabled(true)
        } else {
            setIsEnabled(false)
        }
    }, [username, password, repeatPassword])

  return (
    (user.userId !== null) ? <Navigate to="/" />
    :
    <div className="px-1 py-6 relative flex flex-col items-center justify-between overflow-x-hidden overflow-y-hidden">
        <div className="pt-10 w-11/12 max-w-md">
            <form className="px-5 py-8 rounded-lg form-bg border border-solid border-gray-800">
                <div className="mb-5">
                    <div className="relative">
                        <input
                            value={username}
                            onChange={e => setUsername(e.target.value)} 
                            required 
                            className="input-focus" 
                        />
                        <label className="input-label">Username</label>
                    </div>
                </div>

                <div className="mb-5">
                    <div className="relative">
                        <input 
                            type="password"
                            value={password}
                            onChange={e => setPassword(e.target.value)} 
                            required 
                            className="input-focus" 
                        />
                        <label className="input-label">Password</label>
                    </div>
                </div>

                <div className="mb-16">
                    <div className="relative">
                        <input 
                            type="password"
                            value={repeatPassword}
                            onChange={e => setRepeatPassword(e.target.value)} 
                            required 
                            className="input-focus" 
                        />
                        <label className="input-label">Repeat Password</label>
                    </div>
                </div>

                {
                    isError
                    &&
                    <div className="my-2 text-center text-red-600 text-sm font-normal leading-5">
                        {errorMessage}
                    </div>
                }

                {
                    isEnabled 
                    ?
                    <button
                        onClick={e => register(e)} 
                        className="button-gradient text-sm h-10 leading-5 px-4 w-full border border-transparent items-center rounded-lg text-white inline-flex font-medium justify-center relative text-center"
                    >
                        Create Account
                    </button>
                    :
                    <button
                        disabled
                        onClick={e => register(e)} 
                        className="button-gradient text-sm h-10 leading-5 px-4 w-full border border-transparent items-center rounded-lg text-white inline-flex font-medium justify-center relative text-center opacity-50"
                    >
                        Create Account
                    </button>
                }
            </form>
        </div>
    </div>
  )
}
export default Register