import { useState } from "react"

const DashboardProduct = ({product}) => {
    const { productName, isActive, _id, productId, series, manufacturer, category, price, releaseDate, specifications } = product
    const [productIsActive, setProductIsActive] = useState(isActive)
    const [isUpdating, setIsUpdating] = useState(false)
    const [newProductId, setNewProductId] = useState(productId)
    const [newProductName, setNewProductName] = useState(productName)
    const [newSeries, setNewSeries] = useState(series)
    const [newManufacturer, setNewManufacturer] = useState(manufacturer)
    const [newCategory, setNewCategory] = useState(category)
    const [newPrice, setNewPrice] = useState(price)
    const [newReleaseDate, setNewReleaseDate] = useState(releaseDate)
    const [newSpecifications, setNewSpecifications] = useState(specifications)
    const [isError, setIsError] = useState(false)
    const [errorMessage, setErrorMessage] = useState("")

    const deactivateProduct = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/deactivate`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: false
            })    
        })
        .then(res => res.json())
        .then(data => {
            setProductIsActive(false)
        })
    }

    const activateProduct = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/reactivate`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: false
            })    
        })
        .then(res => res.json())
        .then(data => {
            setProductIsActive(true)
        })
    }

    const updateProduct = (e) => {
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: newProductId,
                productName: newProductName,
                series: newSeries,
                manufacturer: newManufacturer,
                category: newCategory,
                price: newPrice,
                releaseDate: newReleaseDate,
                specifications: newSpecifications
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data.status !== "success") {
                setIsError(true)
                setErrorMessage(data.message)
            } else {
                setIsError(false)
                setIsUpdating(false)
            }
        })
    }

  return (
        isUpdating 
        ?
        <div className="px-1 py-6 relative flex flex-col items-center justify-between overflow-x-hidden overflow-y-hidden">
            <div className="pt-10 w-11/12 max-w-md">
                <form className="px-5 py-8 rounded-lg form-bg border border-solid border-gray-800">
                    <div className="mb-5">
                        <div className="relative">
                            <input
                                type="number"
                                value={newProductId}
                                onChange={e => setNewProductId(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Product ID</label>
                        </div>
                    </div>

                    <div className="mb-5">
                        <div className="relative">
                            <input 
                                type="text"
                                value={newProductName}
                                onChange={e => setNewProductName(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Product Name</label>
                        </div>
                    </div>

                    <div className="mb-5">
                        <div className="relative">
                            <input 
                                type="text"
                                value={newSeries}
                                onChange={e => setNewSeries(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Series</label>
                        </div>
                    </div>

                    <div className="mb-5">
                        <div className="relative">
                            <input 
                                type="text"
                                value={newManufacturer}
                                onChange={e => setNewManufacturer(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Manufacturer</label>
                        </div>
                    </div>

                    <div className="mb-5">
                        <div className="relative">
                            <input 
                                type="text"
                                value={newCategory}
                                onChange={e => setNewCategory(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Category</label>
                        </div>
                    </div>

                    <div className="mb-5">
                        <div className="relative">
                            <input 
                                type="number"
                                value={newPrice}
                                onChange={e => setNewPrice(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Price</label>
                        </div>
                    </div>

                    <div className="mb-5">
                        <div className="relative">
                            <input 
                                type="text"
                                value={newReleaseDate}
                                onChange={e => setNewReleaseDate(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Release Date</label>
                        </div>
                    </div>

                    <div className="mb-16">
                        <div className="relative">
                            <input 
                                type="text"
                                value={newSpecifications}
                                onChange={e => setNewSpecifications(e.target.value)} 
                                required 
                                className="input-focus" 
                            />
                            <label className="input-label">Specifications</label>
                        </div>
                    </div>

                    {
                        isError
                        &&
                        <div className="my-2 text-center text-red-600 text-sm font-normal leading-5">
                            {errorMessage}
                        </div>
                    }

                    <button
                        onClick={e => updateProduct(e)} 
                        className="button-gradient text-sm h-10 leading-5 px-4 w-full border border-transparent items-center rounded-lg text-white inline-flex font-medium justify-center relative text-center"
                    >
                        Update Product
                    </button>
                </form>
            </div>
            </div>
        :
        <div className="dashboard-product-container">
            <span className="dashboard-product-productName" style={{ color: productIsActive ? "#afdb1b" : "#ff433e" }}>{newProductName}</span>
            <button onClick={() => setIsUpdating(true)} className="dashboard-product-button">Update</button>
            {
                productIsActive 
                ?
                <button onClick={deactivateProduct} className="dashboard-product-button">Deactivate</button>
                :
                <button onClick={activateProduct} className="dashboard-product-button">Activate</button>
            }
        </div>
  )
}
export default DashboardProduct