const ProductCard = (product) => {
    const { productId, productName, series, price } = product.product 

  return (
    <div className="product-wrapper">
        <div className="product-card">

            <div className="product-card-main">
                <div className="product-card-tag">
                    <span>#{productId}</span>
                </div>
                <div className="product-card-image-container">
                    <img className="product-card-image" src={ require(`../assets/images/${productId}.png`) || "" } alt={`${productName} img`} />
                </div>
                <div className="product-card-price-container">
                    <div className="product-card-price">
                        <div className="product-card-price-text">
                            ¥{price}
                        </div>
                    </div>
                </div>
            </div>

            <div className="product-card-footer">
                <div className="product-card-description">
                    <div className="product-card-name">{productName}</div>
                    <div className="product-card-series">{series}</div>
                </div>
            </div>
        </div>
    </div>
  )
}
export default ProductCard