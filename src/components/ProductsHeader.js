import { Link } from "react-router-dom"

const ProductsHeader = () => {
  return (
    <div className="products-header">
        <div className="products-header-title-container">
            <nav>
                <ol className="products-header-ol">
                    <li className="products-header-li">
                        <Link to="/products"><span className="products-header-li-span">Nendoroid</span></Link>
                    </li>
                    <li className="products-header-li"></li>
                </ol>
            </nav>
        </div>
    </div>
  )
}
export default ProductsHeader