import { useContext } from "react"
import { Link } from "react-router-dom"
import UserContext from "../context/UserContext"

const Navbar = () => {

    const { user } = useContext(UserContext)

  return (
    <div className="mobile-navbar">
        <div className="mobile-navbar-content">
            <div className="mobile-navbar-hamburger">
                <svg
                    viewBox="0 0 24 24"
                    width={30}
                    height={30}
                    style={{
                    fill: "currentcolor",
                    }}
                >
                    <path
                    d="M7 7a1 1 0 0 0 0 2V7Zm10 2a1 1 0 1 0 0-2v2ZM7 11a1 1 0 1 0 0 2v-2Zm10 2a1 1 0 1 0 0-2v2ZM7 15a1 1 0 1 0 0 2v-2Zm10 2a1 1 0 1 0 0-2v2ZM7 9h10V7H7v2Zm0 4h10v-2H7v2Zm0 4h10v-2H7v2Z"
                    fill="currentColor"
                    />
                </svg>
            </div>
            <Link to='/'>
                <img className="mobile-navbar-logo" src={ require(`../assets/images/logo-no-background.png`) } alt="hamburger icon" />
            </Link>
            {
                user.userId !== null 
                ?
                <Link className="mobile-navbar-logout" to="/logout">Log out</Link>
                :
                <Link className="mobile-navbar-logout" to="/login">Log in</Link>
            }
        </div>
    </div>
  )
}
export default Navbar