import { useContext, useState } from "react"
import { Link } from "react-router-dom"
import UserContext from "../context/UserContext"

const Sidebar = () => {

    const { user } = useContext(UserContext)

    const [expanded, setExpanded] = useState(false)

    const sidebarStyle = expanded ? { width: "320px", flex: "0 0 320px", minWidth: "320px", maxWidth: "320px" } : { width: "72px", flex: "0 0 72px", minWidth: "72px", maxWidth: "72px" }

    const toggleSidebar = () => {
        setExpanded(prev => !prev)
    }

  return (
    <div style={sidebarStyle}>
        <div style={{ height: "100vh" }}>
            <div className="sidebar-container">

                <span onClick={toggleSidebar} className={`${!expanded ? "sidebar-collapse-button-collapsed" : ""} sidebar-collapse-button`}>
                    <svg
                        viewBox="0 0 24 24"
                        width={20}
                        height={20}
                        style={{
                        fill: "currentcolor",
                        }}
                    >
                        <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M12 22c5.523 0 10-4.477 10-10S17.523 2 12 2 2 6.477 2 12s4.477 10 10 10ZM10.47 8.47l-3 3a.75.75 0 0 0 0 1.06l3 3a.75.75 0 1 0 1.06-1.06l-1.72-1.72H16a.75.75 0 0 0 0-1.5H9.81l1.72-1.72a.75.75 0 0 0-1.06-1.06Z"
                        fill="currentColor"
                        />
                    </svg>
                </span>

                <section className={`${!expanded ? "sidebar-logo-collapsed" : ""} sidebar-logo`}>
                    <Link style={{ position: "relative", display: "inline-block" }} to="/">
                        {
                            expanded ?
                            <img src={require('../assets/images/logo-no-background.png')} alt="logo" width={104} />
                            :
                            <img src={require('../assets/images/logo-small.png')} width={40} alt="logo" />
                        }
                    </Link>
                </section>

                <section className={`${!expanded ? "sidebar-links-collapsed" : ""} sidebar-links`}>
                    <ul className="sidebar-menu-links">
                        <li className="sidebar-menu-link-list-item">
                            <Link to="/" className="sidebar-menu-link-item-a">
                                <span className="sidebar-menu-link-icon">
                                    <svg
                                        width={24}
                                        height={24}
                                        style={{
                                        fill: "currentcolor",
                                        }}
                                    >
                                        <path
                                        fillRule="evenodd"
                                        clipRule="evenodd"
                                        d="M21 10.15v7.817C21 20.194 19.21 22 17 22H7c-2.21 0-4-1.806-4-4.033V10.15c0-1.21.54-2.357 1.47-3.123l5-4.118a3.975 3.975 0 0 1 5.06 0l5 4.118A4.046 4.046 0 0 1 21 10.15Zm-5.75 7.1v2.25a1 1 0 0 1-1 1h-4.5a1 1 0 0 1-1-1v-2.25a3.25 3.25 0 0 1 6.5 0Z"
                                        fill="currentColor"
                                        />
                                    </svg>
                                </span>
                                <span className="sidebar-menu-link-label">Home</span>
                            </Link>
                        </li>
                        <li className="sidebar-menu-link-list-item">
                            <Link to="/products" className="sidebar-menu-link-item-a">
                                <span className="sidebar-menu-link-icon">
                                    <svg
                                        width={24}
                                        height={24}
                                        style={{
                                        fill: "currentcolor",
                                        }}
                                    >
                                        <path
                                        fillRule="evenodd"
                                        clipRule="evenodd"
                                        d="M12 1.25A3.75 3.75 0 0 0 8.25 5v1a4 4 0 0 0-3.862 3.263l-1.5 8A4 4 0 0 0 6.82 22h10.36a4 4 0 0 0 3.932-4.737l-1.5-8A4 4 0 0 0 15.75 6V5A3.75 3.75 0 0 0 12 1.25ZM14.25 6V5a2.25 2.25 0 0 0-4.5 0v1h4.5Z"
                                        fill="currentColor"
                                        />
                                    </svg>
                                </span>
                                <span className="sidebar-menu-link-label">Products</span>
                            </Link>
                        </li>
                        {
                            user.isAdmin &&
                            <li>
                                <Link to="/admindashboard" className="sidebar-menu-link-item-a">
                                    <span className="sidebar-menu-link-icon">
                                        <svg
                                            width={24}
                                            height={24}
                                            style={{
                                            fill: "currentcolor",
                                            }}
                                        >
                                            <path
                                            fillRule="evenodd"
                                            clipRule="evenodd"
                                            d="M7.75 2A4.75 4.75 0 0 0 3 6.75v12.5A2.75 2.75 0 0 0 5.75 22h14.5a.75.75 0 0 0 0-1.5H5.75a1.25 1.25 0 1 1 0-2.5h10.5A4.75 4.75 0 0 0 21 13.25v-6.5A4.75 4.75 0 0 0 16.25 2h-8.5Zm3.42 10.915 3.147-1.573c1.105-.553 1.105-2.13 0-2.684L11.17 7.085A1.5 1.5 0 0 0 9 8.427v3.146a1.5 1.5 0 0 0 2.17 1.342Z"
                                            fill="currentColor"
                                            />
                                        </svg>
                                    </span>
                                    <span className="sidebar-menu-link-label">Admin Dashboard</span>
                                </Link>
                            </li>
                        }
                    </ul>
                </section>

                <div className="sidebar-footer">
                    <section className={`${!expanded ? "sidebar-footer-section-collapsed" : ""} sidebar-footer-section`}></section>
                    <div className="sidebar-accountInfo-container">
                        <div className="sidebar-accountInfo-content">
                            <div style={{flex: "1 1"}}>
                                <div style={{ width: "100%", display: "flex", alignItems: "center" }}>

                                    <div style={{ width: "40px", height: "40px", maxWidth: "40px", maxHeight: "40px", cursor: "pointer", marginRight: "8px" }}>
                                        <svg
                                            viewBox="0 0 24 24"
                                            width={40}
                                            height={40}
                                            style={{
                                            fill: "currentcolor",
                                            }}
                                        >
                                            <path
                                            fillRule="evenodd"
                                            clipRule="evenodd"
                                            d="M22 12a9.976 9.976 0 0 1-3.441 7.549A9.961 9.961 0 0 1 12 22a9.961 9.961 0 0 1-6.559-2.451A9.977 9.977 0 0 1 2 12C2 6.477 6.477 2 12 2s10 4.477 10 10Zm-7-3a3 3 0 1 0-6 0 3 3 0 0 0 6 0Zm-3 5c1.713 0 2.839.917 3.633 1.935.593.76.389 1.876-.47 2.312A6.97 6.97 0 0 1 12 19a6.97 6.97 0 0 1-3.162-.753c-.86-.436-1.064-1.551-.471-2.312C9.16 14.917 10.287 14 12 14Z"
                                            fill="currentColor"
                                            />
                                        </svg>
                                    </div>

                                    {/* <div className="sidebar-accountInfo-user">
                                        Miko
                                    </div> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}
export default Sidebar