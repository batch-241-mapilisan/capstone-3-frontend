const CartProduct = ({product}) => {

    const { name, price } = product

  return (
    <div className="dashboard-product-container">
        <span className="dashboard-product-productName">{name}</span>
        <span className="dashboard-product-price">¥{price}</span>
        <button className="dashboard-product-button">Remove</button>
    </div>
  )
}
export default CartProduct