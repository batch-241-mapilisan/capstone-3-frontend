import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Navbar from './components/Navbar';
import Login from './pages/Login';
import Register from './pages/Register';
import Home from './pages/Home';
import { useEffect, useState } from 'react';
import { UserProvider } from './context/UserContext';
import jwtDecode from 'jwt-decode';
import Logout from './pages/Logout';
import Products from './pages/Products';
import ProductPage from './pages/ProductPage';
import Sidebar from './components/Sidebar';
import Admin from './pages/Admin';
import CartButton from './components/CartButton';
import Cart from './pages/Cart';

function App() {
  const [user, setUser] = useState({
    userId: null,
    username: null,
    isAdmin: null
  })
  console.log(user)

  const [cart, setCart] = useState([])
  const [cartTotal, setCartTotal] = useState(cart.length || 0)
  const [totalPrice, setTotalPrice] = useState(0)
  const [cartLength, setCartLength] = useState(cart.length || 0)

  const [windowSize, setWindowSize] = useState({
      width: window.innerWidth,
      height: window.innerHeight
  })

  const setDimension = () => {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight
    })
  }

  useEffect(() => {
      window.addEventListener('resize', setDimension)

      return (() => {
          window.removeEventListener('resize', setDimension)
      })
  }, [windowSize])

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    const token = localStorage.getItem('token')
    if (!token) {
      setUser({
        userId: null,
        username: null,
        isAdmin: null
      })
    } else {
      const decoded = jwtDecode(token)
      const { userId, username, isAdmin } = decoded.UserInfo
      setUser({
        userId,
        username,
        isAdmin
      })
    }
  }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser, cartTotal, setCartTotal, cart, setCart, totalPrice, setTotalPrice, cartLength, setCartLength}}>
      <Router>
        {windowSize.width < 768 ? 
          <>
            <Navbar />
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/products" element={<Products />} />
              <Route path="/products/:id" element={<ProductPage />} />
              <Route path="/admindashboard" element={<Admin />} />
              <Route path="/cart" element={<Cart />} />
            </Routes> 
          </>
          : 
          <div style={{display: 'flex'}}>
            <Sidebar />
            <div style={{ width: "100%" }}>
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/login" element={<Login />} />
                <Route path="/register" element={<Register />} />
                <Route path="/logout" element={<Logout />} />
                <Route path="/products" element={<Products />} />
                <Route path="/products/:id" element={<ProductPage />} />
                <Route path="/admindashboard" element={<Admin />} />
                <Route path="/cart" element={<Cart />} />
              </Routes>
            </div>
          </div>
        }
      <CartButton />
      </Router>
    </UserProvider>
  );
}

export default App;
